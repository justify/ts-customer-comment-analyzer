import { readdirSync } from "fs";
import { join } from "path";
import { forkJoin } from "rxjs";
import { CommentAnalyzerAsync } from "./comment-analyzer-async.class";
import { MetricCommand } from "./metric-command.class";

class MainStatic {
  main() {
    const totalResults = new Map<string, number>();
    const dirPath = join(__dirname, "..", "docs");
    const commentFiles = readdirSync(dirPath).filter((path: string) =>
      path.endsWith(".txt")
    );
    const metricMatchCommands: MetricCommand[] = [
      new MetricCommand((line) => line.length < 15, "SHORTER_THAN_15"),
      new MetricCommand((line) => line.indexOf("Mover") > -1, "MOVER_MENTIONS"),
      new MetricCommand(
        (line) => line.indexOf("Shaker") > -1,
        "SHAKER_MENTIONS"
      ),
      new MetricCommand((line) => line.indexOf("?") > -1, "QUESTIONS"),
      new MetricCommand((line) => {
        const regex = new RegExp(
          /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi
        );
        return !!line.match(regex);
      }, "SPAM"),
    ];

    forkJoin(
      commentFiles.map((commentFile: string) => {
        return new CommentAnalyzerAsync(join(dirPath, commentFile)).analyze(
          metricMatchCommands
        );
      })
    ).subscribe((joined: Map<string, number>[]) => {
      joined.map((fileResults: Map<string, number>) => {
        this.addReportResults(fileResults, totalResults);
      });
      console.log("RESULTS\n=======");
      totalResults.forEach((v: number, k: string) => {
        console.log(`${k} : ${v}`);
      });
    });
  }

  /**
   * This method adds the result counts from a source map to the target map
   * @param source the source map
   * @param target the target map
   */
  private addReportResults(
    source: Map<string, number>,
    target: Map<string, number>
  ): void {
    for (let entry of source.entries()) {
      const [key, value] = entry;
      (!target.has(key) && target.set(key, value)) ||
        target.set(key, target.get(key) + value);
    }
  }
}
export const Main = new MainStatic();

Main.main();
