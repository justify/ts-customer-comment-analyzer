import { readFileSync, existsSync } from "fs";
import { MetricCommand } from "./metric-command.class";

export class CommentAnalyzer {
  constructor(private filePath: string) {}

  analyze(metricMatchCommands: MetricCommand[]): Map<string, number> {
    const resultsMap = new Map<string, number>();
    if (!existsSync(this.filePath)) {
      throw new FileNotFoundException(`File not found: ${this.filePath}`);
    }
    let fileLines: string[];
    try {
      fileLines = readFileSync(this.filePath).toString().split("\n");
    } catch (err) {
      throw new IOException(`IO Error processing file: ${this.filePath}`);
    }

    for (let line of fileLines) {
      for (let command of metricMatchCommands) {
        command.execute(line) &&
          this.incOccurrence(resultsMap, command.metricLabel);
      }
    }
    return resultsMap;
  }

  /**
   * This method increments a counter by 1 for a match type on the countMap. Uninitialized keys will be set to 1
   * @param countMap the map that keeps track of counts
   * @param key the key for the value to increment
   */
  private incOccurrence(countMap: Map<string, number>, key: string): void {
    !countMap.has(key) && countMap.set(key, 0);
    countMap.set(key, countMap.get(key) + 1);
  }
}

export class FileNotFoundException extends Error {
  constructor(message: string) {
    super(message);
  }
}

export class IOException extends Error {
  constructor(message: string) {
    super(message);
  }
}
