export class MetricCommand {
  constructor(
    private op: (line: string) => boolean,
    public metricLabel: string
  ) {}

  execute(line: string): boolean {
    return this.op(line)
  }
}
